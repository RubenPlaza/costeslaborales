﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CostesLaborales
{
    class TrabAprendiz:Trabajador
    {

        /*Un TrabAprendiz  (deriva de Trabajador): es como un trabajador sin nivel, que está aprendiendo, pero le pagan a la hora un porcentaje (int)
         * del sueldo de un trabajador de la misma profesión.*/
        byte porcentaje;

        public TrabAprendiz( int id,string nombre, byte idProfesion,byte porcentaje,byte idObra)
            :base(id,nombre,idProfesion,porcentaje,idObra)
        {
            this.porcentaje = porcentaje;
        }

        public override byte Porcentaje { get => porcentaje; set => porcentaje = value; }

        public override double CalculaCostes(double coste, byte porcentaje)
        {

            return coste * porcentaje/100;
        }


        public override void PresentaTrabajador()
        {

            Console.WriteLine("\t {0}\t{1}\t      {2}\t       {3}%          {4}", IdTrabajador, Util.CuadraTexto(Nombre, 15), IdProfesion, Porcentaje, IdObra);

        }
    }
}
