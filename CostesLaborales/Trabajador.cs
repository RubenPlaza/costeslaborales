﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace CostesLaborales
{
   
    
    class Trabajador
    {

        public static List<Trabajador> listaTrabajadores = new List<Trabajador>();
        int idTrabajador;
        String nombre;                //Siempre será de cuatro cifras. Identifica a cada trabajador => no se puede repetir.     
        byte idProfesion; // se refiere a la profesión del trabajador => un entero del 0 al 4.
        byte nivel;       // es el grado de especialización (un entero del 0 al 4)
        byte idObra;
        byte porcentaje;

        public Trabajador(int idTrabajador, string nombre, byte idProfesion, byte nivel, byte idObra)
        {
            this.idTrabajador = idTrabajador;
            this.nombre = nombre;
            this.idProfesion = idProfesion;
            this.nivel = nivel;
            this.idObra = idObra;
        }

        public int IdTrabajador { get => idTrabajador; set => idTrabajador = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public byte IdProfesion { get => idProfesion; set => idProfesion = value; }
        public byte Nivel { get => nivel; set => nivel = value; }
        public byte IdObra { get => idObra; set => idObra = value; }
        public virtual byte Porcentaje { get => porcentaje; set => porcentaje = value; }

        public virtual double CalculaCostes(double coste, byte nivel)
        {

            return coste * (1 + Nivel * 0.25);
        }
         public virtual  void PresentaTrabajador()
        {
           
            
            
                Console.WriteLine("\t {0}\t{1}\t      {2}\t         {3}          {4}", idTrabajador, Util.CuadraTexto(Nombre, 15), IdProfesion, Nivel,IdObra);
            
        }
    }
        
    }

