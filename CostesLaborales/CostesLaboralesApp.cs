﻿// Apellidos, Nombre:

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace CostesLaborales
{
    //Ruben Plaza Garcia
    class CostesLaboralesApp
    {
        static byte contObras;
        
        static void Main(string[] args)
        {
            leeObras();
            CatalogoProf.leeProfesiones();
            leeTrabajadores();
           
            int opcion = 0;
            bool volverMenu;
            do
            {
                volverMenu = false;
                opcion = Util.MenuPrincipal();
                Console.Clear();
               
                switch (opcion)
                {
                    case 1://gestionar Profesiones
                        while (opcion != 50)
                        {
                            opcion = Util.Menu();
                        Console.Clear();
                       
                            switch (opcion)
                            {
                                case 1:
                                    CatalogoProf.PresentaProfesiones();
                                    break;
                                case 2:
                                    CatalogoProf.AñadirProfesion();
                                    break;
                                case 3:
                                    CatalogoProf.ModificarProfesion();
                                    break;
                                case 4:
                                    CatalogoProf.EliminarProfesion();
                                    break;
                                case 0:
                                    opcion = 50;//esto lo hago para que no salga del menu principal
                                    volverMenu = true;
                                    break;
                                default:
                                    Console.WriteLine("\n\n\n\t\t**   Opción incorrecta   **");
                                    break;
                            }
                            if (opcion != 50)
                                Console.ReadKey();
                        }

                        break;
                    case 2://gestionar trabajadores
                        while (opcion != 50)
                        {
                            opcion = Util.Menu();
                        Console.Clear();
                        switch (opcion) {
                            case 1:
                                Console.WriteLine("\t ID       Nombre       IdProfesion      Nivel     IdObra");
                                Console.WriteLine("\t ---------------------------------------------------------------");
                                foreach (var item in Trabajador.listaTrabajadores)
                                {
                                    item.PresentaTrabajador();
                                }
                                break;
                            case 2:
                                 AltaTrabajador();                              
                                break;
                            case 3:
                                ModificarTrabajador();
                                break;
                            case 4:
                                BajaTrabajador();
                                break;
                            case 0:
                                opcion = 50;
                                volverMenu = true;
                                break;
                                default:
                                    Console.WriteLine("\n\n\n\t\t**   Opción incorrecta   **");
                                    break;
                            }
                            if (opcion != 50)
                                Console.ReadKey();
                        }
                        break;
                    case 3:
                        while (opcion != 50)
                        {
                            opcion = Util.MenuObra();
                        Console.Clear();
                        switch (opcion)
                        {

                            case 1:
                                Obra.PresentaObras();
                                break;
                            case 2:
                                AñadirObra();
                                break;
                            case 3:
                                ModificarObra();
                                break;
                            case 4:
                                EliminarObra();
                                break;
                            case 5:
                                AltaDelTrabajador();
                                break;
                            case 6:
                                BajaDelTrabajador();
                                break;
                            case 7:
                                int idObra = BuscaObra("Costes Totales de la obra?");
                                    if (idObra >= 0)                                 
                                        CostesTotales(Obra.ListaObras[idObra]);                                   
                                    else
                                        Console.WriteLine("\n\tERROR* Obra Erronea");
                                break;                                
                            case 0:
                                opcion = 50;
                                volverMenu = true;
                                break;
                                default:
                                    Console.WriteLine("\n\n\n\t\t**   Opción incorrecta   **");
                                    break;
                            }
                            if (opcion != 50)
                                Console.ReadKey();
                        }
                        break;
                    case 4:
                        GuardarCambios();
                        break;
                    case 0:break;

                    default:
                        Console.WriteLine("\n\n\n\t\t**   Opción incorrecta   **");
                        break;
                }
                if(opcion!=0 && !volverMenu)
                Console.ReadKey();
            } while (opcion != 0);

            Console.WriteLine("\n\n\t\tPulse una tecla para Salir");
            Console.ReadKey();
        }

        private static void GuardarCambios()
        {

                FileStream fw = new FileStream(@"../../Datos/Obras.txt",FileMode.Create);
                StreamWriter sw = new StreamWriter(fw,Encoding.UTF7);
            
                for (int i = 0; i < Obra.ListaObras.Count; i++)
                {
                    sw.WriteLine("{0};{1};{2};{3}",Obra.ListaObras[i].IdObra,Obra.ListaObras[i].Nombre, Obra.ListaObras[i].NumDias,Obra.ListaObras[i].Direccion);
                }
                sw.Close();
                fw= new FileStream(@"../../Datos/Trabajadores.txt", FileMode.Create);
                sw = new StreamWriter(fw,Encoding.UTF7);
                for (int i = 0; i < Trabajador.listaTrabajadores.Count; i++)
                {
                    sw.WriteLine("{0};{1};{2};{3};{4}", Trabajador.listaTrabajadores[i].IdTrabajador, Trabajador.listaTrabajadores[i].Nombre, Trabajador.listaTrabajadores[i].IdProfesion, Trabajador.listaTrabajadores[i].Nivel, Trabajador.listaTrabajadores[i].IdObra);
                }
                sw.Close();
                fw = new FileStream(@"../../Datos/Profesiones.txt", FileMode.Create);
                sw = new StreamWriter(fw, Encoding.UTF7);

                for (int i = 0; i < CatalogoProf.listaProfesiones.Count; i++)
                {
                    sw.WriteLine("{0};{1};{2};{3}", CatalogoProf.listaProfesiones[i].IdProfesion,CatalogoProf.listaProfesiones[i].NombreProf,CatalogoProf.listaProfesiones[i].EurosHoras,CatalogoProf.listaProfesiones[i].HorasDias);
                }
                sw.Close();

                Console.WriteLine("\n\tCambios Guardados");
            
            
        }

        public static void BajaDelTrabajador()
        {
            
           
            int idObra = BuscaObra("Dime la obra a la que pertenece el trabajador");
            if (idObra >=0)
            {
                int cont = 0;
                Console.WriteLine("\n\tTrabajadores obra: ");
                Console.WriteLine("\t-------------------------");
                for (int i = 0; i < Trabajador.listaTrabajadores.Count; i++)
                {
                    if (Trabajador.listaTrabajadores[i].IdObra == idObra)
                    {
                        Console.WriteLine("\t{0}\t{1}", Trabajador.listaTrabajadores[i].IdTrabajador, Trabajador.listaTrabajadores[i].Nombre);
                        cont++;
                    }
                }

                if (cont > 0)
                {
                    int id = BuscaTrabajador("Selecciona el trabajador");
                    if (id >= 0 && Trabajador.listaTrabajadores[id].IdObra > 0)
                    {
                        Trabajador.listaTrabajadores[id].IdObra = 0;
                        Console.WriteLine("\n\t\tTrabajador añadido al Paro.");
                    }
                    else
                        Console.WriteLine("\n\t\tERROR* Trabajador no existe");
                }
                else
                {
                    Console.WriteLine("\n\t\tNo hay trabajadores Trabajando");
                }
            }
            else {
                Console.WriteLine("\n\tERROR* Esa obra no existe");
            }
        }
        public static int BuscaTrabajador(string texto) {

          
            int id = Util.CapturaEntero("\n\t"+texto, 1000, 9999);
            for (int i = 0; i < Trabajador.listaTrabajadores.Count; i++)
            {
                if (id == Trabajador.listaTrabajadores[i].IdTrabajador)
                {
                    return i;
                }
            }
            return -1;
        }
        public static void AltaDelTrabajador()
        {           
            Console.WriteLine("\n\tTrabajadores sin obra: ");
            Console.WriteLine("\t-------------------------");
           int cont = 0;
            for (int i = 0; i < Trabajador.listaTrabajadores.Count; i++)
            {
                if (Trabajador.listaTrabajadores[i].IdObra == 0)
                {
                    Console.WriteLine("\t{0}\t{1}", Trabajador.listaTrabajadores[i].IdTrabajador, Trabajador.listaTrabajadores[i].Nombre);
                    cont++;
                }
            }
           
            if (cont>0)
            {
                int id = BuscaTrabajador("Selecciona el trabajador");

               
                if (id >= 0 && Trabajador.listaTrabajadores[id].IdObra == 0)
                {
                    Console.Clear();

                    int idObra = BuscaObra("A que obra quieres añadirlo?");

                    if (idObra >= 0)
                    {
                        
                        Trabajador.listaTrabajadores[id].IdObra =Convert.ToByte( idObra);
                        Console.WriteLine("\n\t\tTrabajador añadido.");
                    }
                    else
                        Console.WriteLine("\n\t\tERROR* Obra erronea");
                }
                else
                    Console.WriteLine("\n\t\tERROR* Trabajador no existe");
            }
            else {
                Console.WriteLine("\n\t\tNo hay trabajadores en paro");
            }
        }
        public static int BuscaObra(string texto) {
            Obra.PresentaObras();
            
            int id = Util.CapturaEnteroSinLimite("\n\t"+texto);
            for (int i = 0; i < Obra.ListaObras.Count; i++)
            {
                if (id == Obra.ListaObras[i].IdObra)
                {

                    return i;
                }
            }
            return -1;
        }
        public static void EliminarObra()
        {
            int posicion = BuscaObra("Dime la obra a eliminar");
            bool respuesta = false;
            List<int> listaPosiciones = new List<int>();
            if (posicion >= 0)
            {
               
                for (int j = 0; j < Trabajador.listaTrabajadores.Count; j++)
                {
                    if (Trabajador.listaTrabajadores[j].IdObra == posicion)
                    {
                        listaPosiciones.Add(j);
                    }
                }
                respuesta = Util.PreguntaSioNo("\nHay (" + listaPosiciones.Count + ") Trabajadores asociados a esta obra , desea eliminar la obra?");

                if (respuesta)
                {

                    for (int i = 0; i < listaPosiciones.Count; i++)
                    {
                        Trabajador.listaTrabajadores[listaPosiciones[i]].IdObra = 0;
                    }
                    Obra.ListaObras.Remove(Obra.ListaObras[posicion]);
                    Console.WriteLine("\n\n\tObra Eliminada, pulse una tecla para continuar");
                }
                else
                {
                    Console.WriteLine("\n\n\tHas negado la eliminación de la obra, pulse tecla para volver al menu");
                }
            }          
            else
            {
                Console.WriteLine("\n\t*Error* Esa obra no existe");
            }
           
        }
        public static void ModificarObra()
        {

            int id = BuscaObra("Dime la obra a modificar");

            if (id >= 0)
            {
                string nombre = Util.CapturaNombre("Dime el nombre: ", 20);
                Obra.ListaObras[id].Nombre = nombre;
                int numDias = Util.CapturaEnteroSinLimite("Dime el numero de dias");
                Obra.ListaObras[id].NumDias = numDias;
                string direccion = Util.CapturaNombre("Calle de la obra?: ", 40);
                Obra.ListaObras[id].Direccion = direccion;

                Console.WriteLine("\n\n\tObra Modificado, pulse una tecla para continuar");

            }
            else
                Console.WriteLine("\n\t\tERROR* Obra Incorrecta");
        }
        public static void AñadirObra() {
           
            string nombre = Util.CapturaNombre("Dime el nombre de la obra",20);
            int dias = Util.CapturaEnteroSinLimite("Numero de dias de trabajo?");
            string direccion = Util.CapturaNombre("Calle de la obra?: ",40);
            Obra.ListaObras.Add(new Obra(contObras,nombre,dias,direccion));
            Console.WriteLine("\n\n\t\tObra añadida");
            contObras++;

        }
        public static void leeTrabajadores()
        {
            StreamReader sr = new StreamReader(@"../../Datos/Trabajadores.txt",Encoding.UTF7);
           
            int idTrabajador;
            String nombre;
            byte idProfesion;
            byte nivel;
            byte idObra;
            string[] tabCampos;
            string linea;
            while (!sr.EndOfStream)
            {
                linea = sr.ReadLine();
                tabCampos = linea.Split(';');
                idTrabajador = Convert.ToInt32(tabCampos[0]);
                nombre = tabCampos[1];
                idProfesion = Convert.ToByte(tabCampos[2]);
                nivel = Convert.ToByte(tabCampos[3]);
                idObra = Convert.ToByte(tabCampos[4]);
                if (nivel <= 4 && nivel >= 0)
                {
                    Trabajador.listaTrabajadores.Add(new Trabajador(idTrabajador, nombre, idProfesion, nivel, idObra));
                    
                }
                else
                {
                    Trabajador.listaTrabajadores.Add(new TrabAprendiz(idTrabajador, nombre, idProfesion, nivel, idObra));
                    
                }

                


            }
            sr.Close();
        }
        public static void leeObras()
        {

            StreamReader sr = new StreamReader(@"../../Datos/Obras.txt",Encoding.UTF7);
            byte idObra;
            string nombre;
            int numDias;
            string direccion;

            string[] tabCampos;
            string linea;
            while (!sr.EndOfStream)
            {
                linea = sr.ReadLine();
                tabCampos = linea.Split(';');
                idObra = Convert.ToByte(tabCampos[0]);
                nombre = tabCampos[1];
                numDias = Convert.ToByte(tabCampos[2]);
                direccion = tabCampos[3];
                Obra.ListaObras.Add(new Obra(idObra, nombre, numDias, direccion));
                
            }
            contObras =Convert.ToByte(Obra.ListaObras.Count);
            sr.Close();

        }
        public static void CostesTotales(Obra obra)
        {
            Console.WriteLine("\n\n\n\t\tOBRA: {0} ({1}  días)\n", obra.Nombre, obra.NumDias);
            Console.WriteLine("\t id\tPROFESIÓN     BASE  NIVEL   E/HORA  nºHoras     Total");
            Console.WriteLine("\t----\t------------  ----  -----   ------  -------     -----");
            string profesion = string.Empty;
            int costeBase = 0;
            int cont = 1;
            double numerosEurosHoras = 0;
            int numeroHoras = 0;
            double total = 0;
            bool trabajador=false;
            double costesTotales = 0;
            for (int i = 0; i <Trabajador. listaTrabajadores.Count; i++)
            {
                if (Trabajador. listaTrabajadores[i].IdObra == obra.IdObra) {
                    for (int p = 0; p < CatalogoProf.listaProfesiones.Count; p++)
                    {


                        if (Trabajador. listaTrabajadores[i].IdProfesion == cont)
                        {
                            profesion = CatalogoProf.listaProfesiones[p].NombreProf;
                            costeBase = Convert.ToInt32(CatalogoProf.listaProfesiones[p].EurosHoras);
                            numeroHoras = Convert.ToInt32(CatalogoProf.listaProfesiones[p].HorasDias * obra.NumDias);
                            
                            if (Trabajador. listaTrabajadores[i].GetType().ToString() == "CostesLaborales.Trabajador")
                            {
                                numerosEurosHoras =Trabajador. listaTrabajadores[i].CalculaCostes(CatalogoProf.listaProfesiones[p].EurosHoras,Trabajador. listaTrabajadores[i].Nivel);
                                trabajador = true;
                            }
                            else
                            {
                                numerosEurosHoras =Trabajador. listaTrabajadores[i].CalculaCostes(CatalogoProf.listaProfesiones[p].EurosHoras,Trabajador. listaTrabajadores[i].Porcentaje);
                                trabajador = false;

                            }
                            total = numeroHoras * numerosEurosHoras;
                        }
                        cont++;
                    }
                    cont = 1;

                    costesTotales += total;

                    if (trabajador)
                        Console.WriteLine("\t{0}\t{1} {2}     {3}     {4}       {5}      {6}",Trabajador. listaTrabajadores[i].IdTrabajador, Util.CuadraTexto(profesion, 14), costeBase,Trabajador. listaTrabajadores[i].Nivel, numerosEurosHoras.ToString("00.00"), numeroHoras, total);
                    else
                        Console.WriteLine("\t{0}\t{1} {2}   a:{3}%   {4}       {5}      {6}",Trabajador. listaTrabajadores[i].IdTrabajador, Util.CuadraTexto(profesion, 14), costeBase,Trabajador. listaTrabajadores[i].Porcentaje, numerosEurosHoras.ToString("00.00"), numeroHoras, total);
                }

            }
            Console.WriteLine("\t\t\t\t\t\t\t      --------");
            Console.WriteLine("\t\t\t\t\t\t\t\t "+costesTotales);
 

        }
        public static void ModificarTrabajador() {

            Console.WriteLine("\tID       Nombre       IdProfesion      Nivel     IdObra");
            Console.WriteLine("\t---------------------------------------------------------------");
            foreach (var item in Trabajador.listaTrabajadores)
            {
                item.PresentaTrabajador();
            }
            int posicion = BuscaTrabajador("Selecciona un trabajador");
            Console.Clear();
           
           
            if (posicion >= 0) {
                Console.WriteLine("\tID       Nombre       IdProfesion      Nivel     IdObra");
                Console.WriteLine("\t---------------------------------------------------------------");
                Trabajador.listaTrabajadores[posicion].PresentaTrabajador();
                string nombre = Util.CapturaNombre("Dime el nombre: ", 20);
                Trabajador.listaTrabajadores[posicion].Nombre = nombre;
                
                byte idProfesion;
                bool encontrado = false;

                do
                {
                    
                    CatalogoProf.PresentaProfesiones();
                    idProfesion = Convert.ToByte(Util.CapturaEnteroSinLimite("¿Id de su profesion?"));
                    encontrado = false;
                    for (int i = 0; i < CatalogoProf.listaProfesiones.Count; i++)
                    {
                        if (CatalogoProf.listaProfesiones[i].IdProfesion == idProfesion)
                            encontrado = true;
                    }
                    if (!encontrado)
                        Console.WriteLine("\n\t\tERROR* idProfesion falso");
                } while (!encontrado);

                Trabajador.listaTrabajadores[posicion].IdProfesion = idProfesion;

                byte nivel = Convert.ToByte(Util.CapturaEntero("Dime el nivel", 0, 4));

                Trabajador.listaTrabajadores[posicion].Nivel = nivel;
                
                
                Obra.PresentaObras();
                byte idObra;
                do
                {
                     idObra = Convert.ToByte(Util.CapturaEnteroSinLimite("Dime el id de la obra"));
                    encontrado = false;
                    for (int i = 0; i < Obra.ListaObras.Count; i++)
                    {
                        if (Obra.ListaObras[i].IdObra == idObra)
                            encontrado = true;
                    }
                    if (!encontrado)
                        Console.WriteLine("\n\t\tERROR* idObra falso");
                } while (!encontrado);
                Trabajador.listaTrabajadores[posicion].IdObra = idObra;

                Console.WriteLine("\n\n\tTrabajador Modificado, pulse una tecla para continuar");
            }
            else {
                Console.WriteLine("\n\t*Error* Ese trabajador no existe");
            }
       
        }
        public static void BajaTrabajador(){
            Console.WriteLine("\t\tID trabajador");
            Console.WriteLine("\t\t-----------------");
            for (int i = 0; i < Trabajador.listaTrabajadores.Count; i++)
            {
                Console.WriteLine("\t\t    " + Trabajador.listaTrabajadores[i].IdTrabajador);
            }
            int pos = BuscaTrabajador("Selecciona un trabajador");
            if (pos >= 0) {
                Trabajador.listaTrabajadores.Remove(Trabajador.listaTrabajadores[pos]);                      
                       Console.WriteLine("\n\n\tTrabajador Eliminado, pulse una tecla para continuar");
            }
            else
                Console.WriteLine("\n\t*Error* Ese trabajador no existe");
           
        }
        public static void AltaTrabajador() {
            Console.WriteLine("\t\tID trabajador");
            Console.WriteLine("\t\t-----------------");
            for (int i = 0; i < Trabajador.listaTrabajadores.Count; i++)
            {
                Console.WriteLine("\t\t    "+Trabajador.listaTrabajadores[i].IdTrabajador);
            }
           
            int id;
            bool esCorrecto = true;
            do {
                esCorrecto = true;
                 id = Util.CapturaEntero("¿Id del nuevo trabajador?", 1000, 9999);
                for (int i = 0; i < Trabajador.listaTrabajadores.Count; i++)
                {
                    if (id == Trabajador.listaTrabajadores[i].IdTrabajador) {
                        esCorrecto = false;

                    }
                }
                if (!esCorrecto) {
                    Console.WriteLine("\n\tERROR* ID Duplicado");
                    Console.ReadKey();
                }
            } while (!esCorrecto);



            string nombre = Util.CapturaNombre("Dime el nombre del nuevo trabajador",20);
            //aqui mostrar obras
            bool encontrado = false;
            Console.Clear();
            Obra.PresentaObras();
            byte idObra;
            do
            {
                 idObra = Convert.ToByte(Util.CapturaEnteroSinLimite("Dime el id de su obra"));
                 encontrado = false;
                for (int i = 0; i < Obra.ListaObras.Count; i++)
                {
                    if (Obra.ListaObras[i].IdObra == idObra)
                        encontrado = true;
                }
                if (!encontrado)
                    Console.WriteLine("\n\tERROR* idObra falso");
            } while (!encontrado);
            Console.WriteLine();
            for (int i = 0; i < CatalogoProf.listaProfesiones.Count; i++)
            {
                Console.WriteLine("\t"+CatalogoProf.listaProfesiones[i].IdProfesion + ") "+CatalogoProf.listaProfesiones[i].NombreProf);
            }

            byte idProfesion;


            do
            {
                 idProfesion = Convert.ToByte(Util.CapturaEnteroSinLimite("¿Id de su profesion?"));
                encontrado = false;
                for (int i = 0; i < CatalogoProf.listaProfesiones.Count; i++)
                {
                    if (CatalogoProf.listaProfesiones[i].IdProfesion == idProfesion)
                        encontrado = true;
                }
                if (!encontrado)
                    Console.WriteLine("\n\tERROR* idProfesion falso");
            } while (!encontrado);
            bool esAprendiz = Util.PreguntaSioNo("¿Es aprendiz?");
                        
               if (!esAprendiz)
                    {
                        byte especializacion = Convert.ToByte(Util.CapturaEntero("¿Grado Especialización?", 0, 4));
                        Trabajador.listaTrabajadores.Add( new Trabajador(id,nombre, idProfesion, especializacion,idObra));
                Console.WriteLine("\n\n\tTrabajador añadido, pulse una tecla para continuar");
               
            }
            else
                    {
                byte porcentaje = Convert.ToByte(Util.CapturaEntero("¿Porcentaje a recibir?", 10, 90));
                Trabajador.listaTrabajadores.Add(new TrabAprendiz(id, nombre, idProfesion, porcentaje, idObra));
                Console.WriteLine("\n\n\tAprendiz añadido añadido,  pulse una tecla para continuar");
            }

                 
            
        }
    

    }
    
}
