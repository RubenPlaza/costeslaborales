﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace CostesLaborales
{
    class Obra
    {
        
        static public List<Obra> ListaObras = new List<Obra>();
        byte idObra;
        string nombre;
        int numDias;
        string direccion;
       

        public Obra(byte idObra, string nombre, int numDias, string direccion)
        {
            this.idObra = idObra;
            this.nombre = nombre;
            this.numDias = numDias;
            this.direccion = direccion;
            
        }

        public byte IdObra { get => idObra; set => idObra = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public int NumDias { get => numDias; set => numDias = value; }
        public string Direccion { get => direccion; set => direccion = value; }
       

        static public void PresentaObras()
        {
           
            Console.WriteLine("\tID       Nombre          NumDia         Dirección");
            Console.WriteLine("\t----------------------------------------------------------------");
            for (int i = 0; i < ListaObras.Count; i++)
            {
                Console.WriteLine("\t{0}\t{1}{2}\t  {3}", ListaObras[i].IdObra,Util.CuadraTexto( ListaObras[i].Nombre,20),Util.CuadraTexto( Convert.ToString( ListaObras[i].NumDias),6), ListaObras[i].Direccion);
            }
        }



        // ...
        //        List<>



    }
}
