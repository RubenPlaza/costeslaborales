﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace CostesLaborales
{

    class CatalogoProf
    {
        static public List<Profesion> listaProfesiones = new List<Profesion>();
        static public int cont = 0;

        static public void leeProfesiones() {

            StreamReader sr = new StreamReader(@"../../Datos/Profesiones.txt",Encoding.UTF7);
            string nombreProf;
            float horasDias;
            float eurosHoras;
            byte idProfesion;
            string[] tabCampos;
            string linea;
            while (!sr.EndOfStream) {
                linea = sr.ReadLine();
                tabCampos = linea.Split(';');
                idProfesion = Convert.ToByte(tabCampos[0]);
                nombreProf = tabCampos[1];
                eurosHoras = Convert.ToSingle( tabCampos[2]);
                horasDias = Convert.ToSingle(tabCampos[3]);
                listaProfesiones.Add(new Profesion(idProfesion,nombreProf,eurosHoras, horasDias) );

            }
            sr.Close();
            cont = listaProfesiones.Count;
        }

       static public void PresentaProfesiones() {
            Console.WriteLine("\n\n\n\n\t\t                         sueldo    Horas");
            Console.WriteLine("\t\t ID       Profesión       base      /día");
            Console.WriteLine("\t\t --   -----------------   ----   --------");
            for (int i = 0; i < listaProfesiones.Count; i++)
            {
                Console.WriteLine("\t\t {0}\t{1}\t  {2}\t  {3}",listaProfesiones[i].IdProfesion,Util.CuadraTexto( listaProfesiones[i].NombreProf,15), listaProfesiones[i].EurosHoras, listaProfesiones[i].HorasDias);
            }
        }

        static public void AñadirProfesion() {
            cont++;
            byte idProfesion = Convert.ToByte(cont);
            string nombre = Util.CapturaNombre("Dime el nombre de la nueva profesion",20);
            float eurosHoras = Util.CapturaDecimales("Cuanto se pagara por horas?: ",6,20);
            float horasDias = Util.CapturaEntero("Cuantas Horas tendra su jornada laboral?: ",0,8);
            listaProfesiones.Add(new Profesion(idProfesion, nombre, eurosHoras, horasDias));
            Console.Write("\n\t\tProfesion añadida");

        }
        static public void ModificarProfesion()
        {
            bool encontrado = false;
            do
            {
                PresentaProfesiones();

                int id = Util.CapturaEnteroSinLimite("Dime el id de la profesion a modificar");
               


                for (int i = 0; i < listaProfesiones.Count; i++)
                {
                    if (id == listaProfesiones[i].IdProfesion)
                    {
                        string nombre = Util.CapturaNombre("Dime el nombre de la nueva profesion", 20);
                        float eurosHoras = Util.CapturaDecimales("Cuanto se pagara por horas?: ", 6, 20);
                        float horasDias = Util.CapturaEntero("Cuantas Horas tendra su jornada laboral?: ", 0, 8);
                        listaProfesiones[i].NombreProf = nombre;
                        listaProfesiones[i].EurosHoras = eurosHoras;
                        listaProfesiones[i].HorasDias = horasDias;
                        Console.Write("\n\t\tCambios Realizados");
                        encontrado = true;
                    }
                }
                if (!encontrado)
                {
                    Console.Clear();
                    Console.Write("\n\t\tERROR* Esa profesion no existe.");
                    Console.ReadKey();
                    Console.Clear();
                }

            } while (!encontrado);
           

        }
        static public void EliminarProfesion()
        {
            bool encontrado = false;
            do
            {
                PresentaProfesiones();

                int id = Util.CapturaEnteroSinLimite("Dime el id de la profesion a eliminar");
                bool eliminar = false;
                for (int i = 0; i < Trabajador.listaTrabajadores.Count; i++)
                {
                    if (Trabajador.listaTrabajadores[i].IdProfesion == id)
                    {                                               
                        eliminar = true;
                        break;
                    }
                }
                if (eliminar)
                {
                    Console.Write("\n\t\tERROR* Existen trabajadores ligados a esa profesion");
                    break;
                }

                for (int i = 0; i < listaProfesiones.Count; i++)
                {
                    if (id == listaProfesiones[i].IdProfesion)
                    {
                        listaProfesiones.Remove(listaProfesiones[i]);
                        Console.Write("\n\t\tProfesion eliminada");
                        encontrado = true;
                    }
                }
                if (!encontrado)
                {
                    Console.Clear();
                    Console.Write("\n\t\tERROR* Esa profesion no existe.");
                    Console.ReadKey();
                    Console.Clear();
                }


            } while (!encontrado);


        }
    }
}
