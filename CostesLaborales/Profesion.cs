﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CostesLaborales
{
   
    class Profesion
    {
        byte idProfesion;
        string nombreProf;
        float eurosHoras;
        float horasDias;

        public Profesion(byte idProfesion, string nombreProf, float eurosHoras, float horasDias)
        {
            this.idProfesion = idProfesion;
            this.nombreProf = nombreProf;
            this.eurosHoras = eurosHoras;
            this.horasDias = horasDias;
        }

        public byte IdProfesion { get => idProfesion; set => idProfesion = value; }
        public string NombreProf { get => nombreProf; set => nombreProf = value; }
        public float EurosHoras { get => eurosHoras; set => eurosHoras = value; }
        public float HorasDias { get => horasDias; set => horasDias = value; }
    }
}
