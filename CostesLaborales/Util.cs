﻿
using System;
using System.Collections;

namespace CostesLaborales
{
    public class Util
    {
        public static int CapturaEnteroSinLimite(string texto)
        {

            int num;
            bool esCorrecto;
            do
            {
                esCorrecto = true;

                Console.Write("\n\t {0} : ", texto);
                esCorrecto = Int32.TryParse(Console.ReadLine(), out num);
                if (!esCorrecto)
                {

                    Console.WriteLine("\n\tError de FORMATO", 14);
                    Console.ReadKey();
                }
                
            } while (!esCorrecto);

            return num;
        }
        public static float CapturaDecimales(string texto, int min, int max)
        {

            float num;
            bool esCorrecto;
            do
            {


                Console.Write("\n\t {0} [{1}..{2}]: ", texto, min, max);
                esCorrecto = float.TryParse(Console.ReadLine(), out num);
                if (!esCorrecto)
                {

                    Console.WriteLine("\n\tError de FORMATO", 14);
                    Console.ReadKey();
                }
                else if (num < min || num > max)
                {

                    Console.WriteLine("\n\tVALOR FUERA DE RANGO", 14);
                    esCorrecto = false;
                    Console.Beep(400, 400);
                    Console.ReadKey();
                }
            } while (!esCorrecto);

            return num;
        }

        public static string CapturaNombre(string texto, int numChar) {
            string nombre = "";
            do
            {
                Console.Write("\n\t" + texto + " [maximo:" + numChar + " caracteres]: ");
                nombre = Console.ReadLine();

                if(nombre.Length>numChar)
                    Console.WriteLine("\n\t\tError* Demasiado largo");
            } while (nombre.Length > numChar);
            return nombre;
        }

        //----DEVUELVE LA OPCION SELECCIONADA
        public static int MenuObra()
        {
            Console.Clear();
            Console.WriteLine("\n\n\n\t\t\tAlumno:Rubén Plaza García");
            Console.WriteLine("\t\t\t╔════════════════════════════════╗");
            Console.WriteLine("\t\t\t║         MENU  GESTIONES        ║");
            Console.WriteLine("\t\t\t╠════════════════════════════════╣");
            Console.WriteLine("\t\t\t║                                ║");
            Console.WriteLine("\t\t\t║       1) Mostrar               ║");
            Console.WriteLine("\t\t\t║                                ║");
            Console.WriteLine("\t\t\t║       2) Añadir                ║");
            Console.WriteLine("\t\t\t║                                ║");
            Console.WriteLine("\t\t\t║       3) Modificar             ║");
            Console.WriteLine("\t\t\t║                                ║");
            Console.WriteLine("\t\t\t║       4) Eliminar              ║");
            Console.WriteLine("\t\t\t║                                ║");
            Console.WriteLine("\t\t\t║       5)Alta de Trabajador     ║");
            Console.WriteLine("\t\t\t║                                ║");
            Console.WriteLine("\t\t\t║       6)Baja Del Trabajador    ║");
            Console.WriteLine("\t\t\t║                                ║");
            Console.WriteLine("\t\t\t║       7)Mostrar Coste          ║");
            Console.WriteLine("\t\t\t║                                ║");
            Console.WriteLine("\t\t\t║    0) Volver a Menu Principal  ║");
            Console.WriteLine("\t\t\t║                                ║");
            Console.WriteLine("\t\t\t╚════════════════════════════════╝");
            Console.Write("\n\t\t\t    Elige una opción: ");

            return (Console.ReadKey().KeyChar - 48);
            //-- Capturo la pulsación
            //opcion = ...;



        }
        public static int Menu()
        {
            //En cada una de estas opciones te permitirá Mostrar, Añadir, Modificar y Eliminar sus elementos teniendo en cuenta
            //las relaciones entre ellos. Además, en la Obra podrás dar contratar o despedir trabajadores y mostrar el coste. 

            Console.Clear();
            Console.WriteLine("\n\n\n\t\t\tAlumno:Rubén Plaza García");
            Console.WriteLine("\t\t\t╔════════════════════════════════╗");
            Console.WriteLine("\t\t\t║        MENU  GESTIONES         ║");
            Console.WriteLine("\t\t\t╠════════════════════════════════╣");
            Console.WriteLine("\t\t\t║                                ║");
            Console.WriteLine("\t\t\t║       1) Mostrar               ║");
            Console.WriteLine("\t\t\t║                                ║");
            Console.WriteLine("\t\t\t║       2) Añadir                ║");
            Console.WriteLine("\t\t\t║                                ║");
            Console.WriteLine("\t\t\t║       3) Modificar             ║");
            Console.WriteLine("\t\t\t║                                ║");
            Console.WriteLine("\t\t\t║       4) Eliminar              ║");
            Console.WriteLine("\t\t\t║                                ║");
            Console.WriteLine("\t\t\t║    0) Volver a Menu Principal  ║");
            Console.WriteLine("\t\t\t║                                ║");
            Console.WriteLine("\t\t\t╚════════════════════════════════╝");
            Console.Write("\n\t\t\t    Elige una opción: ");

            return (Console.ReadKey().KeyChar - 48);
            //-- Capturo la pulsación
            //opcion = ...;


        }
        public static int MenuPrincipal()
        {
            

            Console.Clear();
            Console.WriteLine("\n\n\n\t\t\tAlumno:Rubén Plaza García");
            Console.WriteLine("\t\t\t╔════════════════════════════════╗");
            Console.WriteLine("\t\t\t║         MENU PRINCIPAL         ║");
            Console.WriteLine("\t\t\t╠════════════════════════════════╣");
            Console.WriteLine("\t\t\t║                                ║");
            Console.WriteLine("\t\t\t║    1) Gestionar Profesiones    ║");
            Console.WriteLine("\t\t\t║                                ║");
            Console.WriteLine("\t\t\t║    2) Gestionar Trabajadores   ║");
            Console.WriteLine("\t\t\t║                                ║");
            Console.WriteLine("\t\t\t║    3) Gestionar Obras          ║");
            Console.WriteLine("\t\t\t║                                ║");
            Console.WriteLine("\t\t\t║    4) Guardar Cambios          ║");
            Console.WriteLine("\t\t\t║                                ║");
            Console.WriteLine("\t\t\t║           0) Salir             ║");
            Console.WriteLine("\t\t\t║                                ║");
            Console.WriteLine("\t\t\t╚════════════════════════════════╝");
            Console.Write("\n\t\t\t    Elige una opción: ");
           
            return (Console.ReadKey().KeyChar - 48);
            //-- Capturo la pulsación
            //opcion = ...;

            
        }

        public static int CapturaEntero(string texto, int min, int max)
        {

            int num;
            bool esCorrecto;
            do
            {

                
                Console.Write("\n\t {0} [{1}..{2}]: ", texto, min, max);
                esCorrecto = Int32.TryParse(Console.ReadLine(), out num);
                if (!esCorrecto)
                {

                    Console.WriteLine("\n\tError de FORMATO", 14);
                    Console.ReadKey();
                }
                else if (num < min || num > max)
                {

                    Console.WriteLine("\n\tVALOR FUERA DE RANGO", 14);
                    esCorrecto = false;
                    Console.Beep(400, 400);
                    Console.ReadKey();
                }
            } while (!esCorrecto);

            return num;
        }

        
        public static bool PreguntaSioNo(string frase)
        {
            bool respuesta = false, esCorrecto = true;
            char letra;
            do
            {
                Console.Write("\n" + frase + " (S/N)");
                letra = Console.ReadKey(true).KeyChar;
                esCorrecto = true;

                if (letra == 'S' || letra == 's')
                {
                    respuesta = true;
                }
                else if (letra == 'N' || letra == 'n')
                {
                    respuesta = false;
                }
                else
                {
                    Console.WriteLine("\nLetra Incorrecta");
                    esCorrecto = false;
                }

            } while (!esCorrecto);
            return respuesta;
        }


        public static string CuadraTexto(string texto, int numChar)
        {

            texto += "                                                   ";
            return texto.Substring(0, numChar);
        }
       

    }
}
